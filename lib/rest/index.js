/**
 * Created by darren on 27/04/2016.
 */
var bodyParser = require('body-parser');

var ETA = require('../eta');
var MYDB =require('../dao');

exports.config = function (app,passengers, drivers) {



    app.use(bodyParser.json());

    app.get('/register',function(req,res){
        var user_id =req.param('id');
        var pwd=req.param('pwd');
            MYDB.register(user_id,pwd,req,res);

    });
    app.get('/login', function (req, res) {

        //MYDB.test();
        MYDB.checkpwd(req,res);
        //console.log(MYDB.checkpwd(user_id,pwd));
        //console.log('password:'+password);
        // if((user_id ==0001)&&(pwd==0004)){
        //     res.send('login succeed');
        //     res.status(200).end();//android
        // }
        // else {
        //     res.send('login fail');
        //     res.status(400).end();//android
        // }
    });

    // var lat = -33.964000;
    // var lon = 151.129819;
    //==eta====
    //  var poins = [
    //      {lat:-33.9741325, lon:151.1211066},
    //      {lat:-33.9379156, lon:151.112891},
    //      {lat:-33.9225973, lon:151.1620925},
    //      {lat:-33.9236697, lon:151.1451997}
    //  ];



    // var drivers = [{role:1, id:"driver1", lat:poins[0].lat, lon:poins[0].lon},
    //     {role:1, id:"driver2", lat:poins[1].lat, lon:poins[1].lon},
    //     {role:1, id:"driver3", lat:poins[2].lat, lon:poins[2].lon},
    //     {role:1, id:"driver4", lat:poins[3].lat, lon:poins[3].lon}
    // ];
    /////==================



    app.get('/eta', function (req, res) {
        // EAT: estimated arrival time
        ETA.getETA(req.query.lat, req.query.lon, drivers,function(information){
            console.log("The final information is: " + JSON.stringify(information)+'\n'+'\n');
            res.send(information);
        });

        //res.send(ETA);

    })
    app.get('/route',function(req,res){
        var route=ETA.getETA(req.query.lat, req.query.lon, drivers);

    })


}
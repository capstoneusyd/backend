/**
 * Created by darren on 27/04/2016.
 */

var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var io_config = require('../socket');
var app_config = require('../rest');
//var eta = require('../eta/eta-test');
//var User = require('../eta/osrm-client-test');
//var lat = User.getUser()[0];
//var lon = User.getUser()[1];

var passengers = [];
var drivers = [];

io_config.config(io, passengers, drivers);
app_config.config(app, passengers, drivers);


require('dns').lookup(require('os').hostname(), function (err, add, fam) {
    console.log("Server started. \n" +
        "Listening on: "+add+"\n");
});

//eta.config(lat,lon,drivers);

server.listen(3000);


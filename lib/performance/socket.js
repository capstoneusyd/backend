/**
 * Created by darren on 2/05/2016.
 */

var socket = require('socket.io-client')(' ');
var poins = [
    {lat:-33.9741325, lon:151.1211066},
    {lat:-33.9379156, lon:151.112891},
    {lat:-33.9225973, lon:151.1620925},
    {lat:-33.9236697, lon:151.1451997}
];
var interval;
var drivers = [{role:1, id:"driver1", lat:poins[0].lat, lon:poins[0].lon},
    {role:1, id:"driver2", lat:poins[1].lat, lon:poins[1].lon},
    {role:1, id:"driver3", lat:poins[2].lat, lon:poins[2].lon},
    {role:1, id:"driver4", lat:poins[3].lat, lon:poins[3].lon}
];
var driver = drivers[process.argv[2]];
var count = 0;
var direction = getRandomArbitrary(0,7);

socket.on('connect', function(){
    console.log('connected');

    socket.emit('login', driver);

    moving();
});


socket.on('event', function(data){});
socket.on('disconnect', function(){
    interval.clearInterval();
});


function moving() {
    interval = setInterval(function () {
        if(count > 150){
            count = 0;
            direction = getRandomArbitrary(0,7);
        }
        switch(direction){
            case 0:
                driver.lat +=0.00002;
                driver.lon +=0.00002;
                break;
            case 1:
                driver.lat -=0.00002;
                driver.lon -=0.00002;
                break;
            case 2:
                driver.lat +=0.00002;
                driver.lon -=0.00002;
                break;
            case 3:
                driver.lat -=0.00002;
                driver.lon +=0.00002;
                break;
            case 4:
                driver.lat +=0.00002;
                break;
            case 5:
                driver.lat -=0.00002;
                break;
            case 6:
                driver.lon +=0.00002;
                break;
            case 7:
                driver.lon -=0.00002;
                break;
        }
        socket.emit('move', driver);
        count++;
    }, 100);
}

function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
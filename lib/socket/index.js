/**
 * Created by darren on 27/04/2016.
 */

exports.config = function (io,passengers, drivers) {
    io.on('connection', function(socket){
        socket.on('login', function (msg) {
            socket.userRole = msg.role;
            socket.userId = msg.id;
            if(msg.role == 0){
                /*
                * join passenger room
                * send passenger driver list
                * */
                socket.join('passengers');
                passengers.push(msg);
                socket.emit('driver_list', drivers);
            }
            if(msg.role == 1){
                /*
                * join driver room
                * broadcast to passengers
                * */
                socket.join('drivers');
                var driver = {};
                driver.id = msg.id;
                driver.lat = msg.lat;
                driver.lon = msg.lon;
                drivers.push(driver);
                socket.to('passengers').emit('driver_add', driver);
            }
        });

        socket.on('move', function (msg) {
            //find corresponding driver
            //update the driver
            // parameter: drivers
            //
            /*
            for(var x in drivers)
            {
                if(drivers[x].id === msg.id){
                    drivers[x].lat = msg.lat;
                    drivers[x].lon = msg.lon;
                }

            }

            console.log(drivers);
            console.log(msg);

            */

            socket.to('passengers').emit('driver_move', msg);
        });

        socket.on('disconnect', function () {
            if(socket.userRole == 0){
                for(var i=0; i<passengers.length; i++){
                    if(passengers[i].id == socket.userId)
                        passengers.splice(i,1);
                }
            }else{
                for(var i=0; i<drivers.length; i++){
                    if(drivers[i].id == socket.userId)
                        drivers.splice(i,1);
                }
                socket.to('passengers').emit('driver_remove', socket.userId);
            }
        });
            
        
    });




}
/**
 * Created by sharon on 12/10/16.
 */

var http = require('http');

var qs = require('querystring');

var sign = '?';
var content = 'geometries=geojson';

exports.router = function(driverId,driverCoords,passengerCoords,successCallback){

    var driverId = driverId;
    var driverString = driverCoords[1] + ',' +driverCoords[0];
    var driverArray = [driverCoords[1],driverCoords[0]];
    var passengerString = passengerCoords[1] + ',' +passengerCoords[0];
    var passengerArray = [passengerCoords[1],passengerCoords[0]];

    var options = {
        hostname: 'router.project-osrm.org',
        //port: 10086,
        path: '/route/v1/driving/' + driverString +';'+ passengerString + sign + content,
        method: 'GET'
    };

    var req = http.request(options, function (res) {
        //console.log('STATUS: ' + res.statusCode);
        //console.log('HEADERS: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');

        var str = '';

        res.on('data', function (chunk) {
            str += chunk;
        });

        res.on('end', function () {
            //console.log('BODY: ' + chunk);
            //console.log(str);
            var data = JSON.parse(str);

            //console.log("The total-time is: " + data.routes[0].duration);
            var waypoints = data.routes[0].geometry.coordinates;
            var path = [];
            for (var i=0, l=waypoints.length; i<l; i++){
                path[i] = waypoints[i];
            }
            path.unshift(driverArray);
            path.push(passengerArray);

            var message = {};
            message.driverId = driverId;
            message.duration = data.routes[0].duration;
            message.path = path;
            //console.log("+++++++"+JSON.stringify(message) +'\n');

            successCallback(message);
        });
    });
    req.on('error', function (e) {
        console.log('problem with request: ' + e.message);
    });
    req.end();
}



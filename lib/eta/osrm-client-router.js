var OSRM = require('osrm-client');

var i1 = [52.519930,13.438640];
var i2 = [52.513191,13.415852];

var testStreet = [52.4224, 13.333086];
var testCoords = [i1,i2];
var traceCoordinates = [[52.542648,13.393252], [52.543079,13.394780], [52.542107,13.397389]];
var traceTimestamps = [1424684612, 1424684616, 1424684620];

var osrm = new OSRM("https://router.project-osrm.org");

exports.router = function(driverId,driverCoords,passengerCoords,successCallback){
    var message = [];
    var driverId = driverId;
    osrm.route({
        coordinates: [driverCoords, passengerCoords]
    }, function(err, response) {

        //console.log(JSON.stringify(response) +"<br>");
        var data = [JSON.stringify(response)];
        console.log(data);
        data = eval("("+data+")");

        console.log("The total-time is: " + data["route_summary"].total_time + '\n');

        var co = data["via_points"];
        
        message[0] = data['route_summary'].total_time;
        message[1] = co;
        message[2] = driverId;
        console.log(message);
        console.log('\n');
        
        successCallback(message);

    });

}




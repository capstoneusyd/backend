/**
 * Created by sharon on 27/10/16.
 */
/**
 * Created by sharon on 12/10/16.
 */

var http = require('http');

var qs = require('querystring');

var sign = '?';
var content = 'geometries=geojson';

exports.router = function(driverId,driverCoords,passengerCoords,successCallback){

    var driverId = driverId;
    var driverString = driverCoords[0] + ',' +driverCoords[1];
    var driverArray = [driverCoords[0],driverCoords[1]];
    var passengerString = passengerCoords[0] + ',' +passengerCoords[1];
    var passengerArray = [passengerCoords[0],passengerCoords[1]];

    var options = {
        hostname: 'maps.googleapis.com',
        path: '/maps/api/distancematrix/json?origins=' + driverString +'&destinations='+ passengerString,
        method: 'GET'
    };

    var req = http.request(options, function (res) {
        res.setEncoding('utf8');
        var str = '';

        res.on('data', function (chunk) {
            str += chunk;
        });

        res.on('end', function () {

            var data = JSON.parse(str);
            // console.log(data);
            // console.log(driverId);
            // console.log("The total-time is: " + data.rows[0].elements[0].duration.value);
            //var waypoints = data.routes[0].geometry.coordinates;
            //var path = [];
            // for (var i=0, l=waypoints.length; i<l; i++){
            //     path[i] = waypoints[i];
            // }
            // path.unshift(driverArray);
            // path.push(passengerArray);

            var message = {};
            message.driverId = driverId;
            message.duration = data.rows[0].elements[0].duration.value;
            message.path = [];
            //console.log("+++++++"+JSON.stringify(message) +'\n');

            successCallback(message);
        });
    });
    req.on('error', function (e) {
        console.log('problem with request: ' + e.message);
    });
    req.end();
}



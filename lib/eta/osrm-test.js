/**
 * Created by sharon on 26/08/2016.
 */

var OSRM = require('osrm-client');

var osrm = new OSRM("https://router.project-osrm.org");



//You can also pass it query paths directly:

osrm.route({
    coordinates: [[13.438640,52.519930], [13.415852,52.513191]],
    steps: true,
    alternatives: false,
    overview: 'simplified',
    geometries: 'polyline'
}, function(response) {
    console.log(JSON.stringify(response));
});


/**
 * Created by sharon on 2/09/2016.
 */

var eta = require('./index');
var server = require('../server');

var begin = new Date().getTime();

// var lat = -33.964000;
// var lon = 151.129819;
var lat = -33.9107250000;
var lon = 151.1555930000;

// var poins = [
//     {lat:-33.9741325, lon:151.1211066},
//     {lat:-33.9379156, lon:151.112891},
//     {lat:-33.9225973, lon:151.1620925},
//     {lat:-33.9236697, lon:151.1451997}
// ];

var poins = [
    {lat:-33.9742170000, lon:151.0920640000},
    {lat:-33.8869930000, lon:151.0403220000},
    {lat:-33.8913090000, lon:151.2432670000},
    {lat:-33.8809990000, lon:151.1800260000}
];


var drivers = [{role:1, id:"driver1", lat:poins[0].lat, lon:poins[0].lon},
    {role:1, id:"driver2", lat:poins[1].lat, lon:poins[1].lon},
    {role:1, id:"driver3", lat:poins[2].lat, lon:poins[2].lon},
    {role:1, id:"driver4", lat:poins[3].lat, lon:poins[3].lon}
];


var max = 256;
var lat = -33.9742180000;
var lon = 151.0920640000;
var drivers = [];
for(var i = 0; i < max; i++){
    var driver = {
        role: 1,
        lat: lat,
        lon: lon
    }
    drivers.push(driver);
    lat += 0.001;
    lon += 0.001;
}


eta.getETA(lat,lon,drivers,function(information){
    console.log("The final information is: " + JSON.stringify(information)+ '\n');
    interval = new Date().getTime() - begin;
    console.log('【process.uptime()】interval: ', interval);
});



/**
 * Created by darren on 4/05/2016.
 */
//var osrm = require('./osrm-client-router');
    
var osrm = require('./osrm-http-router');
var google = require('./google-http-router')

exports.getETA = function (lat,lon,drivers,successCallback2) {

    var nearDriver = [];
    var min_time = 100000000;
    var min_path = [];
    var information = {};
    var driversNum = 0;

   // var drivers = eval("("+drivers+")");
    for(var n in drivers){
        var driverId = drivers[n].id;
        var driverCoords = [drivers[n].lat,drivers[n].lon];
        var passengerCoords = [lat,lon];
        google.router(driverId,driverCoords,passengerCoords,function(message){

            if (message.duration < min_time){
                min_time = message.duration;
                min_path = message.path;
                nearDriver = message.driverId;
            }

            driversNum++;
            if (driversNum == drivers.length){
                information.driverId = nearDriver;
                information.duration = min_time;
                information.path = min_path;

                //information = {"driverId":"driver2","duration":495,"path":[[151.193456, -33.889814],[151.194195, -33.890425],[151.195164, -33.888103],[151.201475, -33.888977],[151.199028, -33.895070],[151.201206, -33.901430],[151.193540, -33.904300],[151.190245, -33.907878],[151.187102, -33.907610]]};

                //console.log("The nearest driver is: " + JSON.stringify(information)+ '\n');
                successCallback2(information);
            }
        });
    }
}

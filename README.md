# Backend #

## Deploy the server ##

1. Download the whole package

2. Go to the root folder using shell

3. makes sure you have npm and node.js installed

4. Run `npm install` 

5. Run `node index.js`

Here is the sample output

`Server started. `

`Listening on: 192.168.31.166`

Now you have server running on `192.168.31.166`


# Driver Script #

1. Go to /lib/simulation/ using shell

2. Run `node driver 0` to start a driver bot. The last number `0` can be changed from `0` to `3` to start bot on different location.